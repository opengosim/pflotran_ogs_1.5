

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE GAS_WATER
      OPTIONS
        HYSTERESIS
        EHYST 0.1
        ANALYTICAL_JACOBIAN
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block

SUBSURFACE

#=========================== discretization ===================================

GRID
  TYPE grdecl hystpc.grdecl
END

#=========================== regression=========================================

REGRESSION
  CELLS
    1
    10 
    20
  /
END

#=========================== times ============================================
TIME
  FINAL_TIME 500 d 
  INITIAL_TIMESTEP_SIZE 2 d
  MINIMUM_TIMESTEP_SIZE 1.0D-10 d
  MAXIMUM_TIMESTEP_SIZE 100 d at 0. d
END

#=========================== material properties ==============================

MATERIAL_PROPERTY formation
  ID 1
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.350d3
  SPECIFIC_HEAT 1.0d3
  THERMAL_CONDUCTIVITY_DRY 1.541d0
  THERMAL_CONDUCTIVITY_WET 1.541d0
  SOIL_COMPRESSIBILITY_FUNCTION QUADRATIC
  SOIL_COMPRESSIBILITY    4.35d-10 ! 1/Pa
  SOIL_REFERENCE_PRESSURE 1.0D5
  CHARACTERISTIC_CURVES ch1
/

#=========================== saturation functions =============================

CHARACTERISTIC_CURVES ch1

 KRW_TABLE swfn_table
 KRG_TABLE sgfn_table

! Water (real water this time)

 TABLE swfn_table
   PRESSURE_UNITS Bar
   SWFN
     0.02   0    10
     1      1    0
   /
 END

 TABLE sgfn_table
  PRESSURE_UNITS Pa
    SGFN
     0.0     0     0
     0.02   0.0    0.0
     0.255  0.15   0.15
     0.51   0.4    0.4
     0.765  0.8    0.8
     0.98   1.0    0
    /
 END

/

CHARACTERISTIC_CURVES ch2

 KRW_TABLE swfn_table
 KRG_TABLE sgfn_table

! Water (real water this time)

 TABLE swfn_table
   PRESSURE_UNITS Bar
   SWFN
     0.02   0    0.1
     1      1    0
   /
 END

 TABLE sgfn_table
  PRESSURE_UNITS Pa
    SGFN
     0.2      0     0
     0.35     0     0
     0.98     1.0   0
    /
 END

/

#=========================== fluid properties =================================

FLUID_PROPERTY 
 DIFFUSION_COEFFICIENT 0.0
/

#=========================== EOSs =============================================

EOS WATER
 SURFACE_DENSITY 1000.0 kg/m^3
END

EOS GAS
  SURFACE_DENSITY 1.0 kg/m^3
  DATABASE hystpc.dat
  FORMULA_WEIGHT 44.01 g/mol
END

#=========================== regions ==========================================

REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20
/
/

#=========================== flow conditions ==================================

FLOW_CONDITION initial_press
 TYPE
   LIQUID_PRESSURE dirichlet
   GAS_IN_LIQUID_MOLE_FRACTION dirichlet
   TEMPERATURE dirichlet
 END
 LIQUID_PRESSURE 1.01D7 Pa
 GAS_IN_LIQUID_MOLE_FRACTION 0.02342
 TEMPERATURE 33.0d0 C
END

#=========================== flow conditions ==================================

WELL_DATA injg
  WELL_TYPE GAS_INJECTOR
  DIAMETER 0.5 m
  INJECTION_ENTHALPY_P 6.4 Bar !min pressure
  INJECTION_ENTHALPY_T 15 C
  BHPL     100.5    Bar
  TARG_GSV 1000000 m^3/day
  CIJK_D   11 1 1 1
  TIME 250 d
  TARG_GSV 0 m^3/day
END

WELL_DATA injw
  WELL_TYPE WATER_INJECTOR
  DIAMETER 0.5 m
  INJECTION_ENTHALPY_P 6.4 Bar !min pressure
  INJECTION_ENTHALPY_T 15 C
  BHPL     100.5   Bar
  CIJK_D   1 1 1 1
  TARG_WSV 0 m^3/day
  TIME 250 d
  TARG_WSV 10000 m^3/day
END

WELL_DATA prod
  WELL_TYPE PRODUCER
  DIAMETER 0.5 m
  BHPL     99.5    Bar
  TARG_WSV 100000 m^3/day
  CIJK_D   20 1 1 1
END

#========================== condition couplers ==============================

INITIAL_CONDITION initial
FLOW_CONDITION initial_press
REGION all
/

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL formation
END

END_SUBSURFACE
