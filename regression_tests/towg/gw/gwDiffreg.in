SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE GAS_WATER
      OPTIONS
        RESERVOIR_DEFAULTS
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block


SUBSURFACE

NEWTON_SOLVER FLOW
  STOL 1.0D-8
  RTOL 1.0D-7
  ITOL 1.0D-7
  MAXIT 20
END
#=========================== regression =======================================
REGRESSION
  CELLS
    1
    2
  /
END


#========================== discretization ===================================
GRID
  TYPE structured
  ORIGIN 0.d0 0.d0 -1200.d0
  NXYZ 2 1 1
  DXYZ
    100.0
    100.0
    100.0
  END
END

#BRINE 1.092d0 MOLAL
#========================== chemistry ===================================
 
#========================== solver options ===================================
TIMESTEPPER FLOW
TS_ACCELERATION 100
/

#========================== times ============================================
TIME
 FINAL_TIME 3650.d0 d
 MAXIMUM_TIMESTEP_SIZE 5.d0 d
 INITIAL_TIMESTEP_SIZE 2.d-4 d
/

#========================== output ============================================
skip
OUTPUT
  TIMES d 0.0 100.0 200.0 300.0 500.0 600.0 700.0 \
          800.0 900.0 1000. 1500 2000 3000 3200 3650.0
  FORMAT HDF5
  PRINT_PRIMAL_GRID
  EXPLICIT_GRID_PRIMAL_GRID_TYPE CELL_CENTERED

  MASS_BALANCE_FILE
   PERIODIC TIMESTEP 10  
  END
  LINEREPT
END
noskip


#=========================== fluid properties =================================
FLUID_PROPERTY 
  DIFFUSION_COEFFICIENT 1.d-3
/

#=========================== material properties ==============================
MATERIAL_PROPERTY mat1
 ID 1
 POROSITY 0.15d0
 ROCK_DENSITY 2.60E3
 !SPECIFIC_HEAT 0.920E3
 !"isothermal:" 
 SPECIFIC_HEAT 0.920E9
 THERMAL_CONDUCTIVITY_DRY 2.51
 THERMAL_CONDUCTIVITY_WET 2.51
 SATURATION_FUNCTION ch1
 PERMEABILITY
 PERM_X 1.0d-13
 PERM_Y 1.0d-13
 PERM_Z 1.0d-13
 /
/

#=============================================== EOS ========================
EOS WATER
  ! shouldn't make a difference as no wells
  SURFACE_DENSITY 1000.0 kg/m^3
END


EOS GAS
  DATABASE gwCO2.dat
  FORMULA_WEIGHT 44.01 g/mol
  ! shouldn't make a difference
  SURFACE_DENSITY 1.0 kg/m^3
END
#============================================================================

#=========================== saturation functions =============================

CHARACTERISTIC_CURVES ch1

 PERMEABILITY_FUNCTION_GAS BURDINE_BC_SL
  LAMBDA 0.457
  ALPHA 5.1d-5
  GAS_RESIDUAL_SATURATION 0.0

  LIQUID_RESIDUAL_SATURATION 0.3d0
  GAS_CONNATE_SATURATION 0.d0
 END

 PERMEABILITY_FUNCTION_WAT BURDINE_BC
  LAMBDA 0.457
  ALPHA 5.1d-5
  WATER_RESIDUAL_SATURATION 0.3d0
  WATER_CONNATE_SATURATION 0.d0
 END

 CAP_PRESSURE_FUNCTION_WG CONSTANT
  CONSTANT_PRESSURE 0.d0 Pa
 END

END


#=========================== regions ==========================================
REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20 
/
/

REGION inj
 BLOCK 1 1 1 1 1 1
END

REGION othercell
 BLOCK 2 2 1 1 1 1
END

REGION prod
  FACE EAST
  BLOCK 2 2 1 1 1 1
END

#=========================== flow conditions ==================================

FLOW_CONDITION initial
  TYPE
    LIQUID_PRESSURE dirichlet
    GAS_IN_LIQUID_MOLE_FRACTION dirichlet
    TEMPERATURE dirichlet
  END
  LIQUID_PRESSURE 2.0D7 Pa
  GAS_IN_LIQUID_MOLE_FRACTION 1.d-6
  TEMPERATURE 45.0d0 C
END

FLOW_CONDITION initial_highc
  TYPE
    LIQUID_PRESSURE dirichlet
    GAS_IN_LIQUID_MOLE_FRACTION dirichlet
    TEMPERATURE dirichlet
  END
  LIQUID_PRESSURE 2.0D7 Pa
  GAS_IN_LIQUID_MOLE_FRACTION 1.d-2
  TEMPERATURE 45.0d0 C
END

#=========================== condition couplers ===============================

INITIAL_CONDITION
FLOW_CONDITION initial_highc
REGION inj
END

INITIAL_CONDITION
FLOW_CONDITION initial
REGION othercell
END

BOUNDARY_CONDITION prod
  FLOW_CONDITION initial
  REGION prod
END


#=========================== stratigraphy couplers ============================
STRATA
REGION all
MATERIAL mat1
END

#=========================== observations ============================
OBSERVATION
 #BOUNDARY_CONDITION boundary condition name
  REGION inj
  VELOCITY
  AT_CELL_CENTER
END
OBSERVATION
 #BOUNDARY_CONDITION boundary condition name
  REGION othercell
  VELOCITY
  AT_CELL_CENTER
END
#======================== region names



END_SUBSURFACE
