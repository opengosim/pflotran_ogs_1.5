# CO2 Injection - quarter spot case
# equilibration with datum in ft - injection in Mton
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE GAS_WATER
      OPTIONS
        RESERVOIR_DEFAULTS
        ISOTHERMAL
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block

SUBSURFACE

NEWTON_SOLVER FLOW
  STOL 1.0D-18
  RTOL 1.0D-7
  ITOL 1.0D-7
  MAXIT 20
END


#========================== discretization ===================================

GRID
  TYPE grdecl gw10.grdecl
END

#========================== regression ===================================

REGRESSION
  CELLS
    1
    300
  /
END

#========================== solver options ===================================
TIMESTEPPER FLOW
TS_ACCELERATION 100
/
#========================== times ============================================
TIME
 FINAL_TIME 500 d
 MAXIMUM_TIMESTEP_SIZE 30.d0 d
 INITIAL_TIMESTEP_SIZE 2.d-1 d
/

#========================== output options ===================================
skip
OUTPUT
  MASS_BALANCE_FILE
   PERIODIC TIMESTEP 10  
  END
  ECLIPSE_FILE
    PERIOD_SUM TIMESTEP 10
    PERIOD_RST TIMESTEP 1
  END
  LINEREPT
END
noskip
#=========================== fluid properties =================================

FLUID_PROPERTY 
 DIFFUSION_COEFFICIENT 4.04d-9
/

#=========================== EOS =================================
BRINE 1.092d0 MOLAL

EOS WATER
 SURFACE_DENSITY 1000.0 kg/m^3
END

EOS GAS
  SURFACE_DENSITY 1.0 kg/m^3
  !CO2_DATABASE gwCO2.dat
  DATABASE gwCO2.dat
  FORMULA_WEIGHT 44.01 g/mol
END

#=========================== material properties ==============================
MATERIAL_PROPERTY mat1
  ID 1
  ROCK_DENSITY 2.60E3
  SPECIFIC_HEAT 0.920E3
  THERMAL_CONDUCTIVITY_DRY 2.51
  THERMAL_CONDUCTIVITY_WET 2.51
  CHARACTERISTIC_CURVES ch1
/
#=========================== saturation functions =============================
#GAS_WATER module requires: Pcwg, Krw, Krg

CHARACTERISTIC_CURVES ch1

 PC_WG CONSTANT
  CONSTANT_PRESSURE 0.d0 Pa
 END

 PERMEABILITY_FUNCTION_WAT BURDINE_BC
   LAMBDA 0.457
   ALPHA 5.1d-5
   WATER_RESIDUAL_SATURATION 0.3d0
   WATER_CONNATE_SATURATION 0.d0
 END

 PERMEABILITY_FUNCTION_GAS BURDINE_BC_SL
   LAMBDA 0.457
   ALPHA 5.1d-5
   GAS_RESIDUAL_SATURATION 0.0  
   LIQUID_RESIDUAL_SATURATION 0.3d0
   GAS_CONNATE_SATURATION 0.d0
 END

 !TEST

END

#=========================== regions ==========================================
REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20 
/
/

#=========================== wells ==================================

WELL_DATA injg
  WELL_TYPE GAS_INJECTOR
  BHPL       1000 Bar
  DIAMETER   0.5 m
  ! tonne can be defines as: tonne, ton, t
  ! Mega tonne can be defined as: Mton, Mt
  TARG_GM 0.365 Mt/year
  CIJK 1 1 1 1  
END

WELL_DATA prod
  WELL_TYPE PRODUCER
  DIAMETER 0.5 m
  BHPL     100   Bar
  TARG_WSV 10000 m^3/day
  CIJK 10 10 1 1  
END

#=========================== flow conditions ==================================

FLOW_CONDITION initial
  
  TYPE
    PRESSURE hydrostatic
   /

  PRESSURE 120.0 Bar
  !DATUM_D  1200.0 m
  DATUM_D  3937.0078 ft

  GAS_IN_LIQUID_MOLE_FRACTION 1.D-6

  RTEMP 45 C

/


#=========================== condition couplers ==============================

INITIAL_CONDITION
FLOW_CONDITION initial
REGION all
END

#========================== stratigraphy couplers ============================
STRATA
REGION all
MATERIAL mat1
END

#======================== region names

END_SUBSURFACE
