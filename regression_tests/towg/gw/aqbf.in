# Aquifer GW case

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE GAS_WATER
      OPTIONS
        RESERVOIR_DEFAULTS
        ISOTHERMAL
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block


SUBSURFACE

#========================== discretization ===================================

GRID
  TYPE grdecl aqbf.grdecl
END

BRINE 1.092d0 MOLAL

#========================== times ============================================
TIME
 FINAL_TIME 400 d
 MAXIMUM_TIMESTEP_SIZE 30.d0 d
 INITIAL_TIMESTEP_SIZE 10 d
/

#========================== output options ===================================

! Remove comments to plot aquifer influx rate and see it change sign
!OUTPUT
!  MASS_BALANCE_FILE
!   PERIODIC TIMESTEP 10  
!  END
!  ECLIPSE_FILE
!    PERIOD_SUM TIMESTEP 1
!    PERIOD_RST TIMESTEP 50
!  END
!  LINEREPT
!END

#=========================== fluid properties =================================

FLUID_PROPERTY 
 DIFFUSION_COEFFICIENT 0.0
 #DIFFUSION_COEFFICIENT 1.d-9
 #DIFFUSION_COEFFICIENT 4.04d-9
/

#=========================== EOS =================================

EOS WATER
 SURFACE_DENSITY 1000.0 kg/m^3
END

EOS GAS
  SURFACE_DENSITY 1.0 kg/m^3
  DATABASE gwCO2.dat
  FORMULA_WEIGHT 44.01 g/mol
END

#=========================== material properties ==============================
MATERIAL_PROPERTY mat1
  ID 1
  ROCK_DENSITY 2.60E3
  SPECIFIC_HEAT 0.920E9
  THERMAL_CONDUCTIVITY_DRY 2.51
  THERMAL_CONDUCTIVITY_WET 2.51
  CHARACTERISTIC_CURVES ch1
/
#=========================== saturation functions =============================
#GAS_WATER module requires: Pcwg, Krw, Krg

CHARACTERISTIC_CURVES ch1

 # CAP_PRESSURE_FUNCTION_WG, alias = PC_W 
 CAP_PRESSURE_FUNCTION_WG CONSTANT
  CONSTANT_PRESSURE 0.d0 Pa
 END

 PERMEABILITY_FUNCTION_WAT BURDINE_BC
   LAMBDA 0.457
   ALPHA 5.1d-5
   WATER_RESIDUAL_SATURATION 0.3d0
   WATER_CONNATE_SATURATION 0.d0
 END

 PERMEABILITY_FUNCTION_GAS BURDINE_BC_SL
   LAMBDA 0.457
   ALPHA 5.1d-5
   GAS_RESIDUAL_SATURATION 0.0  
   LIQUID_RESIDUAL_SATURATION 0.3d0
   GAS_CONNATE_SATURATION 0.d0
 END

 TEST

END

#=========================== regions ==========================================
REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20 
/
/

#=========================== wells ==================================

WELL_DATA prod1
  WELL_TYPE PRODUCER
  BHPL     50  Bar
  CIJK 4 4 1 2 
  TARG_WSV 500 m^3/day
  TIME 200 d 
  TARG_WSV 0 m^3/day
END

WELL_DATA prod2
  WELL_TYPE PRODUCER
  BHPL     50  Bar
  CIJK 4 5 1 2
  TARG_WSV 500 m^3/day
  TIME 200 d 
  TARG_WSV 0 m^3/day
END

WELL_DATA winje1
  WELL_TYPE WATER_INJECTOR
  BHPL     150  Bar
  TIME 200 d
  CIJK 5 4 1 2 
  TARG_WSV 500 m^3/day
END

WELL_DATA winje2
  WELL_TYPE WATER_INJECTOR
  BHPL     150  Bar
  TIME 200 d
  CIJK 5 5 1 2
  TARG_WSV 500 m^3/day
END

AQUIFER_DATA A1
  BACK
  DEPTH     2560.32 m
  THICKNESS 200 m
  RADIUS    2500 m
  PERM      300  mD
  COMPRESSIBILITY 1.0E-9 1/Pa
  GAS_IN_LIQUID_MOLE_FRACTION 0.5D-3
  POROSITY  0.1
  VISCOSITY 1 cP
  CONN_D 1 1 1 1 1 1 X-
  CONN_D 1 1 1 1 2 2 X- 3.0
END 

#=========================== flow conditions ==================================

FLOW_CONDITION initial
 TYPE
   LIQUID_PRESSURE dirichlet
   GAS_IN_LIQUID_MOLE_FRACTION dirichlet
   TEMPERATURE dirichlet
 END
 LIQUID_PRESSURE 1.20D7 Pa
 GAS_IN_LIQUID_MOLE_FRACTION 1.D-9
 TEMPERATURE 45.0d0 C
END

#=========================== condition couplers ==============================

INITIAL_CONDITION
FLOW_CONDITION initial
REGION all
END

#========================== regression data==================================
REGRESSION 
  CELLS
    1
    50
  /
END

#========================== stratigraphy couplers ============================
STRATA
REGION all
MATERIAL mat1
END

#======================== region names

END_SUBSURFACE
