

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE GAS_WATER
      OPTIONS
        RESERVOIR_DEFAULTS
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block

SUBSURFACE

#=========================== discretization ===================================

GRID
  TYPE grdecl column.grdecl
END

#=========================== regression =======================================

REGRESSION
  CELLS
    1
    3
    5
   10
  /
END

#=========================== times ============================================
TIME
  FINAL_TIME 1000 d 
  INITIAL_TIMESTEP_SIZE 0.1 d
  MINIMUM_TIMESTEP_SIZE 1.0D-10 d
  MAXIMUM_TIMESTEP_SIZE 50 d at 0. d
END

#=========================== output options ===================================
skip
OUTPUT
  MASS_BALANCE_FILE
   PERIODIC TIMESTEP 1  
  END
  ECLIPSE_FILE
    PERIOD_SUM TIMESTEP 5
    PERIOD_RST TIME 50 d
    WRITE_DENSITY
  END
  LINEREPT
END
noskip
#=========================== material properties ==============================

MATERIAL_PROPERTY drain
  ID 1
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.350d3
  SPECIFIC_HEAT 1.0d3
  THERMAL_CONDUCTIVITY_DRY 1.541d0
  THERMAL_CONDUCTIVITY_WET 1.541d0
  SOIL_COMPRESSIBILITY_FUNCTION QUADRATIC
  SOIL_COMPRESSIBILITY    4.35d-10 ! 1/Pa
  SOIL_REFERENCE_PRESSURE 1.0D5
  CHARACTERISTIC_CURVES drainage
/

#=========================== saturation functions =============================

CHARACTERISTIC_CURVES drainage

 KRW_TABLE swfn_table
 KRG_TABLE sgfn_table

 TABLE swfn_table
   PRESSURE_UNITS Bar
   external_file SWFN_drainage.dat
 END

 TABLE sgfn_table
  PRESSURE_UNITS Bar
   external_file SGFN_drainage.dat
 END

/

#=========================== fluid properties =================================

FLUID_PROPERTY 
 DIFFUSION_COEFFICIENT 4.04d-9
/


#=========================== EOSs =============================================
!BRINE 4.28 MOLAL

EOS WATER
  SURFACE_DENSITY 1077 kg/m^3  !10% Salt 
  DENSITY IF97
END

EOS GAS
 SURFACE_DENSITY 1.867 kg/m^3
 CO2_DATABASE gwCO2.dat
END

#=========================== regions ==========================================

REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20
/
/

#=========================== flow conditions ==================================
FLOW_CONDITION initial_press
  !Equilibrium with Gas/water contact not allowed. Brine only.
  TYPE
    PRESSURE hydrostatic
  /

  PRESSURE 110.0 Bar  !allow to read pressure as well
  DATUM_D  1100.0 m

  GAS_IN_LIQUID_MOLE_FRACTION 1.D-6

  TEMPERATURE_TABLE
    D_UNITS m
    TEMPERATURE_UNITS C !cannot be otherwise
    RTEMPVD
    1100 29
    1200 35
    /
  END

  SALT_TABLE
    D_UNITS m
    CONCENTRATION_UNITS MOLAL
    SALTVD
     1100 0.95
     1200 4.0
    /
  END

skip
  !below example to define salt concentration using ecl units [kg/sm3]
  SALT_TABLE
    D_UNITS m
    CONCENTRATION_UNITS MASS_CONCENTRATION ![kg/sm3]
    SALTVD
     1100 55.0
     1200 200.0
    /
  END
  !below example to define salt concentration using mass fraction
  SALT_TABLE
    D_UNITS m
    CONCENTRATION_UNITS MASS_FRACTION
    SALTVD
     1100 0.0526
     1200 0.1895
    /
  END
noskip

/

#========================== thermal aquifer ==============================

TBC_DATA tbc_top
  TEMPERATURE 29 C
  CONN_D 1 1 1 1  1  1 Z- ! top face 
END

TBC_DATA tbc_bot
  TEMPERATURE 35 C
  CONN_D 1 1 1 1 10 10 Z+ ! bottom face
END

#========================== condition couplers ==============================

INITIAL_CONDITION initial
FLOW_CONDITION initial_press
REGION all
/

#=========================== stratigraphy couplers ============================

STRATA
  REGION all
  MATERIAL drain
END

END_SUBSURFACE
