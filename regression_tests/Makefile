# Makefile for running standard regression tests
#
# include the make variables from PETSc so we can use PYTHON and do
# some conditional testing, e.g. test if unstructured mesh is available.
#
# If PYTHON defined from petsc is not correct, override it on the
# command line with: make PYTHON=python3.3 test

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules


TEST_MANAGER = regression_tests.py
PFLOTRAN = ../src/pflotran/pflotran

TEST_OPTIONS =

# make PERFORMANCE=true check
ifdef PERFORMANCE
	TEST_OPTIONS += --check-performance
endif

ifdef DRYRUN
	TEST_OPTIONS += --dry-run
endif

ifdef UPDATE
	TEST_OPTIONS += --update
endif

ifdef CHECK
	TEST_OPTIONS += --check-only
endif

ifdef NEW
	TEST_OPTIONS += --new-tests
endif

ifdef BACKTRACE
	TEST_OPTIONS += --backtrace
endif

ifdef TIMEOUT
	TEST_OPTIONS += --timeout $(TIMEOUT)
endif

ifneq ($(strip $(MPIEXEC)),)
	TEST_OPTIONS += --mpiexec "$(MPIEXEC)"
endif

#
# standard tests that are run to verify pflotran is built correctly
#
STANDARD_CFG = \
	ascem/batch/batch.cfg \
	ascem/1d/1d-calcite/1d-calcite.cfg \
	default/column/column.cfg \
	default/reaction_sandbox/reaction_sandbox.cfg \
	default/batch/batch.cfg \
	default/numerical/numerical.cfg \
	default/output/output.cfg \
	default/multicontinuum/multicontinuum.cfg \
	default/velocity/velocity.cfg \
	geothermal_hpt/1D_Calcite/calcite_hpt.cfg\
        ngee/ngee.cfg \
	wipp/nuts/nuts.cfg \
	toil_ims/toil_ims.cfg \
        database_generation/database_generation.cfg

ifneq ($(strip $(HYPRE_LIB)),)
STANDARD_CFG += \
	towg/ims/towg_ims.cfg \
	towg/tl/towg_tl.cfg \
	towg/bo/towg_bo.cfg \
	towg/gw/towg_gw.cfg
endif

#ifneq ($(strip $(PARMETIS_LIB)),)
#STANDARD_CFG += 
#endif

#
# domain specific problems
#
GEOCHEMISTRY_CFG = \
	ascem/1d/1d-calcite/1d-calcite.cfg \
	ascem/batch/batch.cfg \
	default/column/column.cfg \
	default/batch/batch.cfg \
	default/multicontinuum/multicontinuum.cfg \
	geothermal_hpt/1D_Calcite/calcite_hpt.cfg\
        ngee/ngee.cfg \
	wipp/nuts/nuts.cfg

GEOMECHANICS_CFG =

FLOW_CFG = \
  toil_ims/toil_ims.cfg \
	towg/ims/towg_ims.cfg \
	towg/tl/towg_tl.cfg \
	towg/bo/towg_bo.cfg \
	towg/gw/towg_gw.cfg

TRANSPORT_CFG = \
	default/column/column.cfg \
	default/multicontinuum/multicontinuum.cfg \
	default/velocity/velocity.cfg

MESH_CFG =

MFD_CFG = \
	mfd/mfd.cfg

CHECK_CFG = \
	ascem/1d/1d-calcite/1d-calcite.cfg

NOHDF5_CFG = \
	default/column/column.cfg \
	geothermal_hpt/1D_Calcite/calcite_hpt.cfg \
	wipp/nuts/nuts.cfg \
	default/multicontinuum/multicontinuum.cfg \
	ascem/1d/1d-calcite/1d-calcite.cfg \
	default/batch/batch.cfg \
	ascem/batch/batch.cfg

WIPP_CFG = \
	wipp/nuts/nuts.cfg \
        wipp/transport/batch/wipp_batch.cfg 


test : standard

standard :
ifeq ($(strip $(PARMETIS_LIB)),)
	@echo "********************************************************"
	@echo "  PFLOTRAN does not appear to be compiled with Parmetis."
	@echo "  Skipping unstructured mesh tests."
	@echo "********************************************************"
endif
	$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--suite standard standard_parallel \
		--config-files $(STANDARD_CFG)


standard_parallel :
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--suite standard_parallel \
		--config-files $(STANDARD_CFG)

check : 
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--suite standard standard_parallel \
		--config-files $(CHECK_CFG)

geochemistry :
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--config-files $(GEOCHEMISTRY_CFG) --suite geochemistry 

geomechanics :
ifneq ($(strip $(PARMETIS_LIB)),)
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--config-files $(GEOMECHANICS_CFG) --suite geomechanics 
else
	@echo "********************************************************"
	@echo "  PFLOTRAN does not appear to be compiled with Parmetis."
	@echo "  Skipping geomechanics tests."
	@echo "********************************************************"
endif

flow :
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--config-files $(FLOW_CFG) --suite flow

transport :
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--config-files $(TRANSPORT_CFG) --suite transport

mesh :
ifneq ($(strip $(PARMETIS_LIB)),)
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--config-files $(MESH_CFG) --suite mesh
else
	@echo "********************************************************"
	@echo "  PFLOTRAN does not appear to be compiled with Parmetis."
	@echo "  Skipping unstructured mesh tests."
	@echo "********************************************************"
endif

ngee-biogeochemistry : 
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		-r ngee --suite biogeochemistry 

test_mimetic :
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--config-files $(MFD_CFG)

nonhdf5_test :
	$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--suite standard standard_parallel \
		--config-files $(NOHDF5_CFG)

wipp_test :
	-$(PYTHON) $(TEST_MANAGER) -e $(PFLOTRAN) $(TEST_OPTIONS) \
		--config-files $(WIPP_CFG)

#
# Try to cleanup the regression test output.
#
# NOTE: do NOT add a "*.h5" glob. The .h5 extension is used for both
# input and output files, and a simple glob can not distinguish
# between them. Manually add rm commands for each file that needs to be
# removed.
#
clean-tests :
	-find . -type f -name '*.testlog' -print0 | xargs -0 rm
	-find . -type f -name '*.out' -print0 | xargs -0 rm
	-find . -type f -name '*.tec' -print0 | xargs -0 rm
	-find . -type f -name '*.regression' -print0 | xargs -0 rm
	-find . -type f -name '*.stdout' -print0 | xargs -0 rm
	-find . -type f -name '*.old' -print0 | xargs -0 rm
	-find . -type f -name '*~' -print0 | xargs -0 rm
	-find . -type f -name 'tmp-restart-*' -print0 | xargs -0 rm
	-find . -type f -name '*.chk' -print0 | xargs -0 rm
	-find . -type f -name '*.pyc' -print0 | xargs -0 rm
	-find . -type f -name 'sigma*.txt' -print0 | xargs -0 rm 
	-find . -type f -name '*.dpd' -print0 | xargs -0 rm
	-find . -type f -name 'e4d*.log' -print0 | xargs -0 rm
	-find . -type d -name '__pycache__' -print0 | xargs -0 rm -r
	-rm -f mfd/mfd-mas.dat
	-rm -f mfd/mfd.h5
	-rm -f ascem/batch/calcite-kinetics-int.dat
	-rm -f ascem/batch/calcite-kinetics-volume-fractions-int.dat
	-rm -f ascem/batch/general-reaction-int.dat
	-rm -f wipp/nuts/case3-int.dat
	-rm -f wipp/nuts/case4r1_modified-int.dat
	-rm -f wipp/bragflo/wipp_flow/gas_generation_2d-np4.h5
	-rm -f wipp/bragflo/wipp_flow/well_production2-ss_mass-0.dat
	-rm -f wipp/bragflo/wipp_flow/gas_generation_2d.h5
	-rm -f wipp/bragflo/wipp_flow/gas_generation_one_cell.h5
	-rm -f wipp/bragflo/wipp_flow/*.pnl
	-rm -f wipp/wipp_flow/gas_generation/*.pnl 
	-rm -f wipp/wipp_flow/gas_generation/*.h5
	-rm -f default/multicontinuum/0D_heat_MPH-int.dat
	-rm -f default/multicontinuum/0D_heat_TH-int.dat
	-rm -f default/anisothermal/thc_1d.h5
	-rm -f default/anisothermal/th_1d.h5
	-rm -f default/timestepping/20x20_xz.h5
	-rm -f default/output/output_biosphere-0.bio
	-rm -f default/output/output_WIPP_pnl-0.pnl
	-rm -f database_generation/*.dat
