! Intended to test standard generation of a co2 SW database
! In this case do not provide a name so that the default is used

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW Flow
      MODE GAS_WATER
      OPTIONS
        ISOTHERMAL
      /
    / ! end of subsurface_flow
  / ! end of process models
END  !! end simulation block

SUBSURFACE


#========================== regression ========================================

REGRESSION
  CELLS
    1
  /
END

#=========================== discretization ===================================

GRID
TYPE structured
NXYZ 1 1 1

DXYZ
 1.d0
 1.d0
 1.d0
END

END

#=========================== times ============================================

TIME
  FINAL_TIME 0.1 d
  INITIAL_TIMESTEP_SIZE 0.1 d
  MAXIMUM_TIMESTEP_SIZE 0.1 d
END

#=========================== material properties ==============================

MATERIAL_PROPERTY formation
ID 1
POROSITY 0.3d0
#TORTUOSITY 1d-1
TORTUOSITY 1.0
ROCK_DENSITY 2.60E3
SPECIFIC_HEAT 0.920E3
THERMAL_CONDUCTIVITY_DRY 2.51
THERMAL_CONDUCTIVITY_WET 2.51
#ROCK_DENSITY 2.40E3
#SPECIFIC_HEAT 1.280E3
#THERMAL_CONDUCTIVITY_DRY 0.5
#THERMAL_CONDUCTIVITY_WET 0.5
#SATURATION_FUNCTION sf2
  CHARACTERISTIC_CURVES ch1
PERMEABILITY
PERM_X 1.0d-13
PERM_Y 1.0d-13
PERM_Z 1.0d-13
/
/

#=========================== saturation functions =============================

CHARACTERISTIC_CURVES ch1

 KRW_TABLE swfn_table
 KRG_TABLE sgfn_table

 TABLE swfn_table
   PRESSURE_UNITS Bar
   SWFN
     0.0    0    0.4
     0.1    0    0.3
     0.9    1    0.2
     1.0    1    0.1
   /
 END

 TABLE sgfn_table
  PRESSURE_UNITS Bar
    SGFN
     0.0     0     0 
     0.10   0.0    0 
     0.255  0.15   0 
     0.51   0.4    0 
     0.765  0.8    0 
     1.0    1.0    0
    /
 END

/

#=========================== fluid properties =================================

FLUID_PROPERTY
  PHASE LIQUID
  DIFFUSION_COEFFICIENT 2.0d-9
/
FLUID_PROPERTY
  PHASE GAS
  DIFFUSION_COEFFICIENT 2.0d-5
/
BRINE 1.092 MOLAL

#=========================== EOSs =============================================

EOS WATER
 SURFACE_DENSITY 1000.0 kg/m^3
END



! This small database generates quickly
EOS GAS
  SURFACE_DENSITY 1.8d0 kg/m^3
  CO2_SPAN_WAGNER_DB
    PRESSURE_MIN 5.51 MPa
    PRESSURE_MAX 6.51 MPa
    PRESSURE_DELTA 1 Bar
    TEMPERATURE_MIN -55 C
    TEMPERATURE_MAX 45 C
    TEMPERATURE_DELTA 2 C
  END
END


#=========================== regions ==========================================

REGION all
COORDINATES
-1.d20 -1.d20 -1.d20
 1.d20  1.d20  1.d20
/
/

#=========================== flow conditions ==================================


FLOW_CONDITION initial_press                                                          
  TYPE                                                                          
    LIQUID_PRESSURE dirichlet                                                   
    GAS_IN_LIQUID_MOLE_FRACTION dirichlet                                       
    TEMPERATURE dirichlet                                                       
  END                                                                           
  LIQUID_PRESSURE 2.0D7 Pa                                                      
  GAS_IN_LIQUID_MOLE_FRACTION 1.d-6                                             
  TEMPERATURE 15.0d0 C                                                          
END     
#=========================== flow conditions ==================================
#========================== condition couplers ==============================

INITIAL_CONDITION initial
FLOW_CONDITION initial_press
REGION all
/

#=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL formation
END

END_SUBSURFACE
