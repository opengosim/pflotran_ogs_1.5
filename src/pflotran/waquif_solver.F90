module Aquifer_Solver_module

! This solver sets up the current state of an aquifer

#include "petsc/finclude/petscsys.h"

  use petscsys
  use PFLOTRAN_Constants_module
  use Well_Type_class
  use Aquifer_Data_class
  use Option_module
  use PM_TOWG_Aux_module

  implicit none

  private

  PetscReal :: as_gravity                  ! Vertical gravity constant

  PetscInt :: a_nconn     = 0              ! Number of connections
  PetscInt :: a_catype    = 0              ! Type (aquifer or TBC)
  PetscBool:: a_back      = PETSC_FALSE    ! Aquifer backflow allowed
  PetscInt :: as_MPI_errs = 0              ! MPI error count

  PetscReal :: as_mwg  = 0.0 ! Gas molecular weight
  PetscReal :: as_sdg  = 0.0 ! Gas surface density weight
  PetscReal :: as_smvg = 0.0 ! Gas surface molar volume

  PetscReal :: as_mww  = 0.0 ! Water molecular weight
  PetscReal :: as_sdw  = 0.0 ! Water surface density weight
  PetscReal :: as_smvw = 0.0 ! Water surface molar volume

  PetscInt , allocatable :: c_local_id  (:)
  PetscInt , allocatable :: c_ghosted_id(:)

  PetscReal, allocatable :: c_p(:)         ! Connection cell pressures
  PetscReal, allocatable :: c_t(:)         ! Connection cell temperatures

  PetscReal, allocatable :: c_kgdp (:,:)   ! Connection mass density
  PetscReal, allocatable :: c_mdp  (:,:)   ! Connection molar density
  PetscReal, allocatable :: c_sp   (:,:)   ! Connection saturations
  PetscReal, allocatable :: c_hp   (:,:)   ! Connection specific enthalpies

  PetscReal, allocatable :: c_kgdpX(:,:,:) ! Connection mass dens.  wrt Xc
  PetscReal, allocatable :: c_mdpX (:,:,:) ! Connection molar density wrt Xc
  PetscReal, allocatable :: c_spX  (:,:,:) ! Connection saturations wrt Xc
  PetscReal, allocatable :: c_hpX  (:,:,:) ! Connection specific enthalpies wrt Xc

  PetscReal, allocatable :: c_z   (:)      ! Connection cell elevations
  PetscReal, allocatable :: c_treq(:)      ! Connection boundary temperatures
  PetscReal, allocatable :: c_trmc(:)      ! Conn. therm. tran. modulo cndct.
  PetscReal, allocatable :: c_frac(:)      ! Conn fractions (add to 1)
  PetscReal, allocatable :: c_ag(:)        ! Connection aqueous mole fraction of gas
  PetscReal, allocatable :: c_aw(:)        ! Connection aqueous mole fraction of water
  PetscReal, allocatable :: c_agX(:,:)     ! Connection aqueous mole fraction of gas   wrt Xc
  PetscReal, allocatable :: c_awX(:,:)     ! Connection aqueous mole fraction of water wrt Xc

  PetscReal, allocatable :: c_flowX(:,:,:) ! Jacobian elements

  PetscInt :: as_nphase = 0 ! Number of phases
  PetscInt :: as_ipoil  = 0 ! Location of oil     phase
  PetscInt :: as_ipgas  = 0 ! Location of gas     phase
  PetscInt :: as_ipwat  = 0 ! Location of water   phase
  PetscInt :: as_ipslv  = 0 ! Location of solvent phase

  PetscInt :: as_ndof   = 0 ! Number of degrees of freedom/cell

  PetscBool :: a_is_gas_water = PETSC_FALSE ! Is gas-water run flag

  PetscInt :: as_xpr    = 1 ! Location of pressure    solution
  PetscInt :: as_xt     = 0 ! Location of temperature solution

  PetscInt :: as_icompw = 0 ! Location of water equation
  PetscInt :: as_icompg = 0 ! Location of gas   equation
  PetscInt :: as_icomph = 0 ! Location of heat  equation

  PetscBool :: as_gaswat     = PETSC_FALSE ! Is gas-water mode
  PetscBool :: as_isothermal = PETSC_TRUE  ! Is isothermal (init: assume so)

  public :: InitialiseAquiferOrTBC, SolveAquiferOrTBC, UpdateAquiferFlux

contains

! ************************************************************************** !

subroutine InitialiseAquiferOrTBC(aquifer_data, grid, option)
  !
  ! Do aquifer/TBC initialisation, including connection fractions
  !
  ! Author: Dave Ponting
  ! Date  : 06/03/20

  use Aquifer_Data_class
  use Grid_Grdecl_module, only : FindWellIndex
  use Grid_module
  use Material_Aux_class
  use EOS_Gas_module
  use EOS_Water_module
  use PM_TOilIms_Aux_module, only : toil_ims_isothermal
  use PM_TOWG_Aux_module, only: towg_isothermal

  implicit none

  type (aquifer_data_type), pointer :: aquifer_data
  type (grid_type)        , pointer :: grid
  type (option_type)      , pointer :: option

  PetscInt  :: iconn, ia, local_id, ghosted_id, iphase
  PetscReal :: total, area, ufct, cz, ct
  PetscBool :: onproc

  ! Set gas-water flag

  as_gaswat = PETSC_FALSE
  if ( (option%iflowmode      == TOWG_MODE     ) .and. &
       (option%iflow_sub_mode == TOWG_GAS_WATER) ) then
    as_gaswat = PETSC_TRUE
  endif

  ! Set isothermal flag

  if (option%iflowmode == TOWG_MODE) then
    as_isothermal = towg_isothermal
  elseif (option%iflowmode == TOIL_IMS_MODE) then
    as_isothermal = toil_ims_isothermal
  else
    as_isothermal = option%use_isothermal
  endif

  ! Set up gravity and aquifer/TBC flag

  as_gravity = abs(option%gravity(3))
  a_catype   = aquifer_data%a_catype

  !  Set up surface density and molecular weights

  do iphase = 1, option%nphase

    if (iphase == option%gas_phase    ) then
      as_mwg  = EOSGasGetFMW()
      as_sdg  = EOSGasGetSurfaceDensity()
      if ( as_sdg> 0.0 ) as_smvg = as_mwg/as_sdg
    endif

    if (iphase == option%liquid_phase ) then
      as_mww = FMWH2O
      as_sdw = EOSWaterGetSurfaceDensity()
      if ( as_sdw> 0.0 ) as_smvw = as_mww/as_sdw
    endif

  enddo

  ! Setup aquifer time constant and inflow coefficient

  if ( a_catype == IS_AQU_TYPE ) then
    call SetupAquiferParameters(aquifer_data)
  endif

  ! Find total area*ufct over this proc

  total = 0.0

  a_nconn = aquifer_data%a_nconn
  do iconn = 1, a_nconn
    area = aquifer_data%a_conn(iconn)%area
    ufct = aquifer_data%a_conn(iconn)%ufct
    total = total + area*ufct
  enddo

  ! Normalise the fractions

  if ( total> 0.0 ) then
    do iconn = 1, a_nconn
      area = aquifer_data%a_conn(iconn)%area
      ufct = aquifer_data%a_conn(iconn)%ufct
      aquifer_data%a_conn(iconn)%frac = area*ufct/total
    enddo
  endif

  ! Loop over completions marking the off-proc ones for deletion

  do iconn = 1, a_nconn

    ! Get the connection location

    ia = aquifer_data%a_conn(iconn)%ia

    call getAquiferLocalAndGhostedIDs(ia, local_id, ghosted_id, onproc, grid)

    aquifer_data%a_conn(iconn)%local_id   = local_id
    aquifer_data%a_conn(iconn)%ghosted_id = ghosted_id
    aquifer_data%a_conn(iconn)%onproc     = onproc

    ! if not on-proc then mark the completion for deletion

    aquifer_data%a_conn(iconn)%mfd = PETSC_FALSE
    if (.not.onproc) then
      aquifer_data%a_conn(iconn)%mfd = PETSC_TRUE
    endif
  enddo

  ! Now delete the marked completions and trim the lists

  if ( a_nconn>0 ) then
    call DeleteMarkedConnections(aquifer_data, option)
  endif
  a_nconn = aquifer_data%a_nconn

  ! Allocate the Jacobian store

  as_ndof = option%nflowdof
  if ( a_nconn>0 ) then
    allocate(aquifer_data%a_connflowX(a_nconn,as_ndof,as_ndof))
    aquifer_data%a_connflowX = 0.0
  endif

  ! Set aquifer pressure to the initial aquifer pressure

  if ( aquifer_data%a_pinit_set ) then
    aquifer_data%a_pd = aquifer_data%a_pinit
    aquifer_data%a_pt = aquifer_data%a_pinit
  endif

  ! Set the connection temperatures for both aquifers and TBCs

  do iconn = 1, a_nconn
    if ( aquifer_data%has_tvd_table ) then
      ! Case of TEMPVD table
      cz = aquifer_data%a_conn(iconn)%cz
      ct = Lookup(cz,aquifer_data%tvd_z,aquifer_data%tvd_t,aquifer_data%ntvdrow)
      aquifer_data%a_conn(iconn)%ctemp = ct
    else
      ! Case of TEMP value
      aquifer_data%a_conn(iconn)%ctemp = aquifer_data%a_t
    endif
  enddo

end subroutine InitialiseAquiferOrTBC

! *************************************************************************** !

subroutine SolveAquiferOrTBC(patch, aux, option, aquifer_data, r_p,&
                             thermal_conductivity)
  !
  ! Supervise aquifer or TBC solution
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  use Patch_module
  use Auxiliary_module
  use PM_TOWG_Aux_module
  use PM_TOilIms_Aux_module

  implicit none

  type(patch_type), pointer :: patch
  type(auxiliary_type) :: aux
  type(option_type), pointer :: option
  type(aquifer_data_type), pointer :: aquifer_data
  PetscReal, pointer :: r_p(:)
  PetscReal, pointer :: thermal_conductivity(:,:)

  PetscReal :: frate
  PetscInt  :: iconn, ierr, ir, ix
  PetscReal :: crate(option%nflowdof)

  ! Initialisation

  ierr  = 0
  frate = 0.0
  crate = 0.0

  ! Number of phases, location of phases, number of degrees of freedom

  as_nphase = option%nphase
  as_ipoil  = option%oil_phase
  as_ipgas  = option%gas_phase
  as_ipwat  = option%liquid_phase
  as_ipslv  = option%solvent_phase
  as_ndof   = option%nflowdof

  ! Location of equations

  as_icompw = as_ipwat          ! Is same as phase location (first)
  as_icompg = as_ipgas          ! Is same as phase location
  as_icomph = option%nphase + 1 ! Is last location

  ! Location of variables

  if (option%iflowmode == TOWG_MODE) then
    as_xpr = TOWG_OIL_PRESSURE_DOF
    as_xt  = towg_energy_dof
  else if (option%iflowmode == TOIL_IMS_MODE) then
    as_xpr = TOIL_IMS_PRESSURE_DOF
    as_xt  = TOIL_IMS_ENERGY_DOF
  else
    call throwWGAException('Aquifers only available in TOWG or TOIL_IMS mode')
  endif

  ! Set up solver values used in both modes

  a_catype = aquifer_data%a_catype
  a_nconn  = aquifer_data%a_nconn
  a_back   = aquifer_data%a_back

  a_is_gas_water=PETSC_FALSE
  if ( towg_miscibility_model == TOWG_GAS_WATER) a_is_gas_water=PETSC_TRUE

  ! Do required set up operations for connections

  if ( a_nconn > 0 ) then

    ! Allocate the work arrays

    call AllocateWorkArrays()

    ! Load the work arrays from the aquifer connection structure

    do iconn = 1, a_nconn
      c_ghosted_id(iconn) = aquifer_data%a_conn(iconn)%ghosted_id
      c_local_id  (iconn) = aquifer_data%a_conn(iconn)%local_id
      c_trmc      (iconn) = aquifer_data%a_conn(iconn)%trmc
      c_frac      (iconn) = aquifer_data%a_conn(iconn)%frac
      c_treq      (iconn) = aquifer_data%a_conn(iconn)%ctemp
      c_z         (iconn) = aquifer_data%a_conn(iconn)%cz
    enddo

    !  Initialise Jacobian

    c_flowX = 0.0

    ! Load the work arrays from cell values

    call AquiferSolverLoader(aux, option)

  endif

  ! Do first-solve-only call on all procs (so can globalise pinit for aquifers)

  if ( .not. aquifer_data%a_solved ) then
    if ( a_catype == IS_AQU_TYPE ) then
      call AquiferSolver0(option, aquifer_data)
    endif
    aquifer_data%a_solved =PETSC_TRUE
  endif

  ! Do the solve only on procs with connections and set up the rates

  if ( a_nconn > 0 ) then

    if ( a_catype == IS_AQU_TYPE ) then
      ! For aquifers
      call AquiferSolver(option, aquifer_data, r_p, frate, crate)
    endif

    if ( a_catype == IS_TBC_TYPE ) then
      ! For TBCs
      call TBCSolver(patch, option, r_p, thermal_conductivity)
    endif

    ! Distribute the Jacobian and deallocate the completion arrays

    do iconn = 1, a_nconn
      do ir = 1, as_ndof
        do ix = 1, as_ndof
          aquifer_data%a_connflowX(iconn,ir,ix) = c_flowX(iconn,ir,ix)
        enddo
      enddo
    enddo

    ! Deallocate the work arrays

    call DeallocateWorkArrays()

  endif

  ! Do some aquifer operations on all procs (so that result known on proc 0)

  if ( a_catype == IS_AQU_TYPE ) then
    call AquiferPostCalculations(option, aquifer_data, frate, crate)
  endif

end subroutine SolveAquiferOrTBC

! *************************************************************************** !

subroutine AquiferSolver0(option, aquifer_data)

  !
  ! Supervise first-solve-only aquifer operations
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  use Auxiliary_module
  use PM_TOWG_Aux_module
  use PM_TOilIms_Aux_module

  implicit none

  type(option_type), pointer :: option
  type(aquifer_data_type), pointer :: aquifer_data

  PetscInt  :: iconn, ierr
  PetscReal :: pinit, pinit_l(1), pinit_g(1), cpaz, az
  PetscMPIInt :: int_mpi

  ! Initialise

  ierr = 0

  ! Load pressures into aquifer_data

  do iconn = 1, a_nconn
    aquifer_data%a_conn(iconn)%cpres = c_p(iconn)
    if ( .not. as_isothermal ) then
      aquifer_data%a_conn(iconn)%ctemp = c_t(iconn)
    endif
  enddo

  ! Find the initial aquifer pressure if not set

  if ( .not.aquifer_data%a_pinit_set ) then

    ! Loop over connections, generating the max. pressure to prevent backflow

    pinit = 0.0
    az    = aquifer_data%a_z

    do iconn = 1, a_nconn

     ! Cell pres. at aquifer depth (if az<cz, aquif. deeper, pressure greater)

      cpaz = c_p(iconn) + as_gravity*c_kgdp(iconn,as_ipwat)*(c_z(iconn) - az)
      pinit = max(cpaz, pinit)

    enddo

    ! Globalise to all procs

    pinit_l(1) = pinit
    pinit_g(1) = pinit
    int_mpi = 1

    call MPI_AllReduce(pinit_l,pinit_g, &
                       int_mpi,MPI_DOUBLE_PRECISION,MPI_MAX,option%mycomm,ierr)
    call checkErr(ierr)

    pinit = pinit_g(1)

    aquifer_data%a_pinit     = pinit
    aquifer_data%a_pt        = pinit
    aquifer_data%a_pd        = pinit
    aquifer_data%a_pinit_set = PETSC_TRUE

  endif

end subroutine AquiferSolver0

! *************************************************************************** !

subroutine AquiferSolver(option, aquifer_data, r_p, frate, crate)

  !
  ! Supervise aquifer solution
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  use Auxiliary_module
  use PM_TOWG_Aux_module
  use PM_TOilIms_Aux_module
  use EOS_Water_module

  implicit none

  type(option_type), pointer :: option
  type(aquifer_data_type), pointer :: aquifer_data
  PetscReal, pointer :: r_p(:)
  PetscReal, intent(out) :: frate
  PetscReal, intent(out) :: crate(option%nflowdof)

  PetscInt  :: iconn, istart, iend, &
               local_id ,ghost_id, ix, ierr
  PetscReal :: res(option%nflowdof), tt_dl, td_dl, p_dl, dpdt, atc, aic,  &
               ap0, td, tt, den, deni, num, flux, frac, &
               f, fp, fd, nump, numd, apt, apd, apinit
  PetscReal :: cpaz, az, md, &
               at, ah, kgdwdum, mdwp, mdwt, atci, p, t, &
               m_nacl, v, xmfnacl, ag

  ! Initialise

  ierr  = 0
  frate = 0.0
  crate = 0.0

  ! Set up aquifer values

  atc     = aquifer_data%a_atc
  atci    = 0.0
  if ( atc > 0.0 ) atci = 1.0/atc
  aic     = aquifer_data%a_aic

  apinit  = aquifer_data%a_pinit
  apt     = aquifer_data%a_pt
  apd     = aquifer_data%a_pd
  at      = aquifer_data%a_t
  ag      = aquifer_data%a_amfg
  ah      = 0.0

  az      = aquifer_data%a_z

  flux    = aquifer_data%a_vflux_total

  !  Set up times and dimensionless times

  td = option%time + option%flow_dt
  tt = option%time

  td_dl = td/atc
  tt_dl = tt/atc

  !  Get dimensionless pressure at T+DT

  p_dl  = GetInfluenceFunctionValue(td_dl,dpdt,aquifer_data)

  ! Set flow expression denominator

  den  = p_dl - tt_dl*dpdt
  deni = 0.0
  if ( den /= 0.0 ) deni = 1.0/den

  ! Set up aquifer pressure

  ap0 = aquifer_data%a_pinit

  ! Find upstream water enthalpy and molar density using time T aquifer values

  call EOSWaterDensity(at, apt, kgdwdum, md, mdwp, mdwt, ierr)
  if ( as_gaswat ) then
    m_nacl  = option%m_nacl
    v       = m_nacl*FMWNACL
    xmfnacl = v/(1.0D3 + v)
    call EOSBrineDensity(at, apt, ag, xmfnacl, md)
  endif
  if ( .not. as_isothermal ) then
    call EOSWaterEnthalpy(at, apt, ah, ierr)
    ah  = ah  * option%scale ! J/kmol -> MJ/kmol
  endif

  ! Loop over connections, generating the inflows

  do iconn = 1, a_nconn

    ! Get the connection location and fraction

    local_id = c_local_id  (iconn)
    ghost_id = c_ghosted_id(iconn)
    frac     = c_frac      (iconn)

    aquifer_data%a_conn(iconn)%cpres = c_p(iconn)
    if ( .not. as_isothermal ) then
      aquifer_data%a_conn(iconn)%ctemp = c_t(iconn)
    endif

    ! Set cell properties

    p = c_p(iconn)
    t = c_t(iconn)

    ! Cell pres. at aquifer depth (if az<cz, aquif. deeper, pressure greater)

    cpaz = p + as_gravity*c_kgdp(iconn,as_ipwat)*(c_z(iconn) - az)

    ! Numerator and derivatives

    num  =  aic*(ap0 - cpaz) - flux*dpdt
    nump = -aic
    numd = -aic*as_gravity*(c_z(iconn) - az)

    ! Obtain resv. vol. rate for this cell and add to total

    f  = frac*atci*num *deni
    fp = frac*atci*nump*deni
    fd = frac*atci*numd*deni
    frate = frate + f

    ! Form residual and update Jacobian

    res = 0.0

    if ( f>0.0 ) then
      call getAquiferFlow( option,aquifer_data,f ,fp ,fd ,md ,ah &
                          ,res ,crate ,iconn)
    else
      ! Only consider reverse flow if BACK option used
      if ( a_back ) then
        call getAquiferBackflow( option,aquifer_data,f ,fp ,fd &
                                ,res ,crate , iconn)
      endif
    endif

    ! Update residual

    iend = local_id*option%nflowdof
    istart = iend-option%nflowdof+1

    r_p(istart:iend) = r_p(istart:iend) + res(1:option%nflowdof)

  enddo

end subroutine AquiferSolver

! *************************************************************************** !

subroutine getAquiferFlow(option,aquifer_data,f ,fp ,fd ,md ,ah, res, crate, iconn)

  !
  ! Find normal aquifer flow (from aquifer into reservoir)
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  implicit none

  type(option_type), pointer :: option
  type(aquifer_data_type), pointer :: aquifer_data

  PetscReal,intent(in) :: f, fp, fd, md, ah
  PetscReal :: amfg, amfw
  PetscReal :: fm ,fmp, fmd, fh, fhp, fhd
  PetscReal,intent(inout) :: res(option%nflowdof)
  PetscReal,intent(inout) :: crate(option%nflowdof)
  PetscInt,intent(in) :: iconn
  PetscInt :: ix

  ! Find the molar rate

  amfg = 0.0
  amfw = 1.0

  if ( as_gaswat ) then
    amfg = aquifer_data%a_amfg
    amfw = 1.0 - amfg
  endif

  fm  = f *md
  fmp = fp*md
  fmd = fd*md

  fh  = fm *ah
  fhp = fmp*ah
  fhd = fmd*ah

  if ( as_gaswat ) then

    ! Gas-water case

    ! Add to residual

    res(as_icompw) = -amfw*fm
    res(as_icompg) = -amfg*fm

    ! Store molar rates
    crate(as_icompw) = crate(as_icompw) + amfw*fm
    crate(as_icompg) = crate(as_icompg) + amfg*fm

    ! Add to Jacobian
    c_flowX(iconn,as_icompw,as_xpr) = -amfw*fmp
    c_flowX(iconn,as_icompg,as_xpr) = -amfg*fmp

    do ix = 1, as_ndof

      c_flowX(iconn,as_icompw,ix) = &
      c_flowX(iconn,as_icompw,ix) - amfw*fmd*c_kgdpX(iconn,as_ipwat,ix)

      c_flowX(iconn,as_icompg,ix) = &
      c_flowX(iconn,as_icompg,ix) - amfg*fmd*c_kgdpX(iconn,as_ipwat,ix)

    enddo

  else

    ! Water only case

    ! Add to residual
    res(as_icompw) = -fm

    ! Store molar rates
    crate(as_icompw) = crate(as_icompw) + fm

    ! Add to Jacobian
    c_flowX(iconn,as_icompw,as_xpr) = -fmp

    do ix = 1, as_ndof
      c_flowX(iconn,as_icompw,ix) = &
      c_flowX(iconn,as_icompw,ix) - fmd*c_kgdpX(iconn,as_ipwat,ix)
    enddo

  endif

  ! Heat flow

  if ( .not. as_isothermal ) then

    ! Add to residual
    res(as_icomph) = -fh

    ! Store rate
    crate(as_icomph) = crate(as_icomph) + fh

    ! Add to Jacobian
    c_flowX(iconn,as_icomph,as_xpr) = -fhp

    do ix = 1, as_ndof
      c_flowX(iconn,as_icomph,ix) = &
      c_flowX(iconn,as_icomph,ix) - fhd*c_kgdpX(iconn,as_ipwat,ix)
   enddo

 endif

end subroutine getAquiferFlow

! *************************************************************************** !

subroutine getAquiferBackflow(option,aquifer_data,f ,fp ,fd , res, crate, iconn)

  !
  ! Find aquifer backflow (from reservoir into aquifer)
  !
  ! Author: Dave Ponting
  ! Date  : 11/11/20

  implicit none

  type(option_type), pointer :: option
  type(aquifer_data_type), pointer :: aquifer_data

  PetscReal,intent(in) :: f, fp, fd
  PetscReal :: amfg, amfw, sw, mdw, hdw
  PetscReal,intent(inout) :: res(option%nflowdof)
  PetscReal,intent(inout) :: crate(option%nflowdof)
  PetscInt,intent(in) :: iconn
  PetscInt  :: ix
  PetscReal :: fm, fmp ,fmkgd ,fms, fmd
  PetscReal :: fh, fhp ,fhkgd ,fhs, fhd, fhh

  ! Find the aquifer mole fractions

  amfg = 0.0
  amfw = 1.0

  if ( as_gaswat ) then
    amfg = c_ag(iconn)
    amfw = c_aw(iconn)
  endif

  ! Set up connection cell Sw, molar denisity and molar emphalpy

  sw  = c_sp(iconn,as_ipwat)
  mdw = c_mdp(iconn,as_ipwat)
  hdw = c_hp (iconn,as_ipwat)

  ! Form the molar flow and derivatives

  fm    = f *sw*mdw
  fmp   = fp*sw*mdw   ! Wrt P
  fmkgd = fd*sw*mdw   ! Wrt mass density
  fms   = f    *mdw   ! Wrt saturation
  fmd   = f *sw       ! Wrt molar density

  ! Form the heat flow and derivatives

  fh    = f *sw*mdw*hdw
  fhp   = fp*sw*mdw*hdw ! Wrt P
  fhkgd = fd*sw*mdw*hdw ! Wrt mass density
  fhs   = f    *mdw*hdw ! Wrt Wrt saturation
  fhd   = f *sw    *hdw ! Wrt molar density
  fhh   = f *sw*mdw     ! Wrt molar enthalpy

  if ( as_gaswat ) then

    ! Gas-water case

    ! Add to residual

    res(as_icompw) = -amfw*fm
    res(as_icompg) = -amfg*fm

    ! Store molar rates
    crate(as_icompw) = crate(as_icompw) + amfw*fm
    crate(as_icompg) = crate(as_icompg) + amfg*fm

    ! Add to Jacobian (pressure terms)
    c_flowX(iconn,as_icompw,as_xpr) = -amfw*fmp
    c_flowX(iconn,as_icompg,as_xpr) = -amfg*fmp

    ! Add to Jacobian (other terms)
    do ix = 1, as_ndof

        c_flowX(iconn,as_icompw,ix)              &
      = c_flowX(iconn,as_icompw,ix)              &
      - amfw*( fmkgd*c_kgdpX(iconn,as_ipwat,ix)  &
              +fms  *c_spX  (iconn,as_ipwat,ix)  &
              +fmd  *c_mdpX (iconn,as_ipwat,ix)) &
      - fm  * c_awX(iconn,ix)

        c_flowX(iconn,as_icompg,ix)              &
      = c_flowX(iconn,as_icompg,ix)              &
      - amfg*( fmkgd*c_kgdpX(iconn,as_ipwat,ix)  &
              +fms  *c_spX  (iconn,as_ipwat,ix)  &
              +fmd  *c_mdpX (iconn,as_ipwat,ix)) &
      - fm  * c_agX(iconn,ix)
    enddo

  else

    ! Water only case

    ! Add to residual
    res(as_icompw) = -fm

    ! Store molar rates
    crate(as_icompw) = crate(as_icompw) + fm

    ! Add to Jacobian
    c_flowX(iconn,as_icompw,as_xpr) = -fmp

    do ix = 1, as_ndof
       c_flowX(iconn,as_icompw,ix)         &
     = c_flowX(iconn,as_icompw,ix)         &
     - ( fmkgd*c_kgdpX(iconn,as_ipwat,ix)  &
        +fms  *c_spX  (iconn,as_ipwat,ix)  &
        +fmd  *c_mdpX (iconn,as_ipwat,ix) )
    enddo

  endif

  ! Heat flow

  if ( .not. as_isothermal ) then

    ! Add to residual
    res(as_icomph) = -fh

    ! Store rate
    crate(as_icomph) = crate(as_icomph) + fh

    ! Add to Jacobian (pressure term)
    c_flowX(iconn,as_icomph,as_xpr) = -fhp

    ! Add to Jacobian (other terms)
    do ix = 1, as_ndof
       c_flowX(iconn,as_icomph,ix)         &
     = c_flowX(iconn,as_icomph,ix)         &
     - ( fhkgd*c_kgdpX(iconn,as_ipwat,ix)  &
        +fhs  *c_spX  (iconn,as_ipwat,ix)  &
        +fhd  *c_mdpX (iconn,as_ipwat,ix)  &
        +fhh  *c_hpX  (iconn,as_ipwat,ix) )
   enddo

 endif

end subroutine getAquiferBackflow

! *************************************************************************** !

subroutine AquiferPostCalculations(option, aquifer_data, frate, crate)

  !
  ! calculations of all procs after main aquifer solution
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  use Patch_module
  use Auxiliary_module
  use PM_TOWG_Aux_module
  use PM_TOilIms_Aux_module

  implicit none

  type(option_type), pointer :: option
  type(aquifer_data_type), pointer :: aquifer_data
  PetscReal,intent(inout) :: frate
  PetscReal,intent(inout) :: crate(option%nflowdof)

  PetscInt  :: ierr
  PetscReal :: frate_l, frate_g, ap, delta_ap, aic, td, tt, &
               td_dl, tt_dl, p_dl, atc, dpdt, den, flux
  PetscReal :: crate_l(option%nflowdof), crate_g(option%nflowdof)
  PetscMPIInt :: int_mpi

  ! Globalise aquifer volume rate

  frate_l = frate
  frate_g = frate
  int_mpi = 1
  ierr    = 0 

  call MPI_AllReduce(frate_l,frate_g, &
                     int_mpi,MPI_DOUBLE_PRECISION,MPI_SUM,option%mycomm,ierr)
  call checkErr(ierr)
  frate = frate_g

  ! Globalise aquifer component molar rates

  crate_l = crate
  crate_g = crate
  int_mpi = option%nflowdof
  ierr    = 0 

  call MPI_AllReduce(crate_l,crate_g, &
                     int_mpi,MPI_DOUBLE_PRECISION,MPI_SUM,option%mycomm,ierr)
  call checkErr(ierr)
  crate = crate_g

  !  Set up times and dimensionless times

  td = option%time + option%flow_dt
  tt = option%time

  atc   = aquifer_data%a_atc
  flux  = aquifer_data%a_vflux_total

  td_dl = td/atc
  tt_dl = tt/atc

  !  Get dimensionless pressure

  p_dl  = GetInfluenceFunctionValue(td_dl,dpdt,aquifer_data)

  ! Set up the a and b coefficient denominators

  den  = p_dl - tt_dl*dpdt

  ! Find the new aquifer pressure

  ap = aquifer_data%a_pinit
  delta_ap = 0.0

  aic = aquifer_data%a_aic
  if ( aic>0.0 ) then
    delta_ap = -(frate*atc*den + flux*dpdt)/aic
  endif

  ap = ap + delta_ap

  ! Store the total aquifer rate and pressure

  aquifer_data%a_vflux_rate = frate
  if ( as_icompw > 0 ) then
    aquifer_data%a_cflux_rate(as_icompw) = crate(as_icompw)*as_smvw
  endif
  if ( as_icompg > 0 ) then
    aquifer_data%a_cflux_rate(as_icompg) = crate(as_icompg)*as_smvg
  endif
  aquifer_data%a_pd = ap

end subroutine AquiferPostCalculations

! *************************************************************************** !

subroutine SetupAquiferParameters(aquifer_data)
  !
  ! Set up aquifer time constant and inflow coefficient
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  implicit none

  type (aquifer_data_type), pointer :: aquifer_data

  PetscReal :: poro, perm, cmpr, thick, inrad, width, theta_frac, visc, inrad2
  PetscBool :: is_linear 

  poro       = aquifer_data%a_poro
  perm       = aquifer_data%a_perm
  cmpr       = aquifer_data%a_cmpr

  thick      = aquifer_data%a_thick
  inrad      = aquifer_data%a_inrad
  width      = aquifer_data%a_width
  theta_frac = aquifer_data%a_theta_frac

  visc       = aquifer_data%a_visc

  inrad2     = inrad*inrad

  is_linear  = aquifer_data%a_is_linear

  ! Set qquifer time constant and aquifer inflow constant

  if ( is_linear ) then
    aquifer_data%a_atc = visc*poro*cmpr/perm
    aquifer_data%a_aic = thick*theta_frac*poro*cmpr*width
  else
    aquifer_data%a_atc = visc*poro*cmpr*inrad2/perm
    aquifer_data%a_aic = 2.0*PI*thick*theta_frac*poro*cmpr*inrad2
  endif

end subroutine SetupAquiferParameters

! *************************************************************************** !

subroutine TBCSolver(patch, option, r_p, thermal_conductivity)

  !
  ! Supervise TBC solution
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  use Patch_module
  use Auxiliary_module
  use PM_TOWG_Aux_module
  use PM_TOilIms_Aux_module

  implicit none

  type(patch_type), pointer :: patch
  type(option_type), pointer :: option
  PetscReal, pointer :: r_p(:)
  PetscReal, pointer :: thermal_conductivity(:,:)

  PetscInt  :: iconn, istart, iend, &
               local_id ,ghost_id, imat
  PetscReal :: res(option%nflowdof), tcond, trmc, frac

  ! Loop over connections, generating the inflows

  do iconn = 1, a_nconn

    res = 0.0

    ! Get the connection location and fraction

    local_id = c_local_id  (iconn)
    ghost_id = c_ghosted_id(iconn)
    frac     = c_frac      (iconn)

    ! Form the thermal conductivity and convert units

    imat     = patch%imat(ghost_id)
    tcond    = thermal_conductivity(1,imat)* 1.d-6 ! j/s -> mj/s
    trmc     = c_trmc(iconn)

    ! Add to residual

    res(as_icomph) = -(c_treq(iconn) - c_t(iconn))*trmc*tcond

    ! Add to Jacobian

    c_flowX(iconn,as_icomph,as_xt) = trmc*tcond

    !  Update global residual

    iend = local_id*option%nflowdof
    istart = iend-option%nflowdof+1

    r_p(istart:iend) = r_p(istart:iend) + res(1:option%nflowdof)

  enddo

end subroutine TBCSolver

! *************************************************************************** !

subroutine UpdateAquiferFlux(option, aquifer_data)
  !
  ! Update the aquifer rate at the end of a step
  !
  ! Author: Dave Ponting
  ! Date  : 05/11/20

  use Patch_module
  use Auxiliary_module

  implicit none

  type(option_type), pointer :: option
  type(aquifer_data_type), pointer :: aquifer_data

  PetscReal :: dt

  if ( aquifer_data%a_solved ) then

    ! Increment the fluxes

    dt = option%flow_dt

    aquifer_data%a_vflux_total = &
    aquifer_data%a_vflux_total + dt*aquifer_data%a_vflux_rate

    aquifer_data%a_cflux_total = &
    aquifer_data%a_cflux_total + dt*aquifer_data%a_cflux_rate

    ! Update the pressure

    aquifer_data%a_pt = aquifer_data%a_pd

  endif

end subroutine UpdateAquiferFlux

! *************************************************************************** !

subroutine AquiferSolverLoader(aux, option)
  !
  ! Load completion solution arrays for TOWG mode
  !
  ! Author: Dave Ponting
  ! Date  : 09/19/18

#include "petsc/finclude/petscsys.h"
  use petscsys
  use Auxiliary_module
  use PM_TOWG_Aux_module
  use PM_TOilIms_Aux_module

  implicit none

  type(auxiliary_type) :: aux
  type(option_type), pointer :: option

  PetscInt :: iconn, ghosted_id

  if (option%iflowmode == TOWG_MODE) then
    do iconn = 1, a_nconn
      ghosted_id = c_ghosted_id(iconn)
      call loadCellDataTOWG(aux%TOWG%auxvars(ZERO_INTEGER, ghosted_id), &
                            option, iconn)
    enddo
  endif

  if (option%iflowmode == TOIL_IMS_MODE) then
    do iconn = 1, a_nconn
      ghosted_id = c_ghosted_id(iconn)
      call loadCellDataTOIL(aux%TOil_ims%auxvars(ZERO_INTEGER, ghosted_id), &
                            option, iconn)
    enddo
  endif

end subroutine AquiferSolverLoader

! *************************************************************************** !

subroutine loadCellDataTOWG(auxvar, option, iconn)
  !
  ! Load completion cell arrays for TOWG mode (for one cell)
  !
  ! Author: Dave Ponting
  ! Date  : 09/19/18

  use AuxVars_TOWG_module

  implicit none

  type(auxvar_towg_type) :: auxvar
  type(option_type), pointer :: option
  PetscInt, intent(in) :: iconn
  PetscInt :: iphase, idof, icgas, icwat

  if (a_is_gas_water) then
    c_p(iconn) = auxvar%pres(as_ipgas)
  else
    c_p(iconn) = auxvar%pres(as_ipoil)
  endif

  c_t (iconn) = auxvar%temp

  do iphase = 1, as_nphase

    c_kgdp(iconn, iphase) = auxvar%den_kg(iphase)
    c_mdp (iconn, iphase) = auxvar%den   (iphase)
    c_sp  (iconn, iphase) = auxvar%sat   (iphase)
    c_hp  (iconn, iphase) = auxvar%H      (iphase)

    if (.not.option%flow%numerical_derivatives) then

      do idof = 1, as_ndof
        c_kgdpX(iconn, iphase, idof) = auxvar%D_den_kg(iphase, idof)
        c_mdpX (iconn, iphase, idof) = auxvar%D_den   (iphase, idof)
        c_spX  (iconn, iphase, idof) = auxvar%D_sat   (iphase, idof)
        c_hpX  (iconn, iphase, idof) = auxvar%D_H     (iphase, idof)
        if (as_isothermal .and. (idof == towg_energy_dof)) then
          c_kgdpX(iconn, iphase, idof) = 0.0
          c_mdpX (iconn, iphase, idof) = 0.0
          c_spX  (iconn, iphase, idof) = 0.0
          c_hpX  (iconn, iphase, idof) = 0.0
        endif
      enddo

    endif

  enddo

!  Specials for gas-water (mole fractions)

  if (as_gaswat) then
    icgas = TOWG_WG_GAS_EQ_IDX
    icwat = TOWG_LIQ_EQ_IDX

    c_ag(iconn) = auxvar%gw%xmol(icgas,as_ipwat)
    c_aw(iconn) = auxvar%gw%xmol(icwat,as_ipwat)

    if (.not.option%flow%numerical_derivatives) then
      do idof = 1, as_ndof
        c_agX(iconn,idof) = auxvar%gw%D_xmol(icgas,as_ipwat,idof)
        c_awX(iconn,idof) = auxvar%gw%D_xmol(icwat,as_ipwat,idof)
      enddo
    endif
  endif

end subroutine loadCellDataTOWG

! *************************************************************************** !

subroutine loadCellDataTOIL(auxvar, option, iconn)
  !
  ! Load completion cell arrays for TOIL mode (for one cell)
  !
  ! Author: Dave Ponting
  ! Date  : 09/19/18

  use AuxVars_TOilIms_module

  implicit none

  type(auxvar_toil_ims_type) :: auxvar
  type(option_type), pointer :: option
  PetscInt, intent(in) :: iconn

  PetscInt :: iphase, idof

  c_p(iconn) = auxvar%pres(as_ipoil)
  c_t(iconn) = auxvar%temp

  do iphase = 1, as_nphase

    c_kgdp(iconn, iphase) = auxvar%den_kg(iphase)

    if (.not.option%flow%numerical_derivatives) then
      do idof = 1, as_ndof
        c_kgdpX(iconn, iphase, idof) = auxvar%D_den_kg(iphase, idof)
      enddo
    endif

  enddo

end subroutine loadCellDataTOIL

! *************************************************************************** !

subroutine AllocateWorkArrays()

  !
  ! Allocate work arrays
  !
  ! Author: Dave Ponting
  ! Date  : 09/19/18

  implicit none

  allocate(c_local_id  (a_nconn))
  allocate(c_ghosted_id(a_nconn))

  allocate(c_p    (a_nconn))
  allocate(c_t    (a_nconn))
  allocate(c_z    (a_nconn))

  allocate(c_kgdp (a_nconn, as_nphase))
  allocate(c_kgdpX(a_nconn, as_nphase, as_ndof))

  allocate(c_mdp  (a_nconn, as_nphase))
  allocate(c_mdpX (a_nconn, as_nphase, as_ndof))

  allocate(c_sp   (a_nconn, as_nphase))
  allocate(c_spX  (a_nconn, as_nphase, as_ndof))

  allocate(c_hp   (a_nconn, as_nphase))
  allocate(c_hpX  (a_nconn, as_nphase, as_ndof))

  allocate(c_trmc (a_nconn))
  allocate(c_frac (a_nconn))
  allocate(c_treq (a_nconn))

  if( as_gaswat ) then

    allocate(c_ag (a_nconn))
    allocate(c_aw (a_nconn))
    allocate(c_agX(a_nconn,as_ndof))
    allocate(c_awX(a_nconn,as_ndof))

  endif

  allocate(c_flowX(a_nconn,as_ndof,as_ndof))

end subroutine AllocateWorkArrays

! *************************************************************************** !

subroutine DeallocateWorkArrays()

  !
  ! Deallocate work arrays
  !
  ! Author: Dave Ponting
  ! Date  : 09/19/18

  implicit none

  deallocate(c_local_id  )
  deallocate(c_ghosted_id)

  deallocate(c_p    )
  deallocate(c_t    )
  deallocate(c_z    )

  deallocate(c_kgdp )
  deallocate(c_kgdpX)

  deallocate(c_mdp  )
  deallocate(c_mdpX )

  deallocate(c_sp   )
  deallocate(c_spX  )

  deallocate(c_hp   )
  deallocate(c_hpX  )

  deallocate(c_trmc )
  deallocate(c_frac )
  deallocate(c_flowX)
  deallocate(c_treq )

  if( as_gaswat ) then
    deallocate(c_ag)
    deallocate(c_aw)
    deallocate(c_agX)
    deallocate(c_awX)
  endif

end subroutine DeallocateWorkArrays

! *************************************************************************** !

subroutine getAquiferLocalAndGhostedIDs(natural_id, local_id, &
                                        ghosted_id, onproc, grid)
  !
  ! Find a completion from its natural id
  ! This needs a search
  !
  ! Author: Dave Ponting
  ! Date: 10/23/18

use Grid_module

  implicit none

  PetscInt,  intent(in)  :: natural_id
  PetscInt,  intent(out) :: local_id
  PetscInt,  intent(out) :: ghosted_id
  PetscBool, intent(out) :: onproc
  type(grid_type), pointer :: grid

  PetscInt :: il, ig, ia, ighost

  ! Initial values

  local_id   = 1
  ghosted_id = 1
  onproc     = PETSC_FALSE

  ! Search local cells

  do il = 1, grid%nlmax
    ig = grid%nL2G(il)
    ia = grid%nG2A(ig)
    if (ia == natural_id) then
      local_id   = il
      ghosted_id = ig
      onproc = PETSC_TRUE
      exit
    endif
  enddo

  ! Search other cells

  if (.not.onproc) then
    do ighost = 1, grid%ngmax
      ia = grid%nG2A(ighost)
      if (ia == natural_id) then
        local_id   = -1
        ghosted_id = ighost
        exit
       endif
    enddo
  endif

end subroutine getAquiferLocalAndGhostedIDs

! *************************************************************************** !

function Lookup(z,az,at,nrow)
  !
  ! Lookup a table

  ! Author: Dave Ponting
  ! Date: 05/05/20

  implicit none

  PetscReal :: Lookup

  PetscReal, intent(in ) :: z
  PetscReal, intent(in ) :: az(nrow)
  PetscReal, intent(in ) :: at(nrow)
  PetscInt , intent(in ) :: nrow

  PetscInt  :: irowl,irowu
  PetscReal :: t, zl, tl, zu, tu, fl, fu, zll, zuu, tll, tuu
  PetscBool :: increasing, decreasing

  t  = at(1) ! Safety case for one row table

  zll = az(1)
  tll = at(1)

  zuu = az(nrow)
  tuu = at(nrow)

  increasing = PETSC_FALSE
  decreasing = PETSC_FALSE

  if ( zll <= zuu ) then
    increasing = PETSC_TRUE ! zl less    than zu (normal table)
  else
    decreasing = PETSC_TRUE ! zl greater than zu (inverted table)
  endif

  if ( ( increasing .and. z <= zll ) .or. &
       ( decreasing .and. z >= zll ) ) then
    ! Before the first value (below first if increasing, above first if decreasing)
    t = at(1)
  else if ( ( increasing .and. z >= zuu ) .or. &
            ( decreasing .and. z <= zuu ) ) then
    ! After the last value   (above last  if increasing, below last  if decreasing)
    t = at(nrow)
  else
    do irowu=2,nrow
      irowl = irowu-1

      zl = az(irowl)
      zu = az(irowu)

      tl = at(irowl)
      tu = at(irowu)

      fu = (z-zl)/(zu-zl)
      if ( (fu>=0.0) .and. (fu<=1.0) ) then
        fl = 1.0 -fu
        t = fl*tl + fu*tu
        exit
      endif
    enddo

  endif

  Lookup = t

end function Lookup

! *************************************************************************** !

subroutine EOSBrineDensity(t, p, ag, xmfnacl, mdb)
  !
  ! Routine to obtain brine molar density
  !
  ! Author: Dave Ponting
  ! Date  : 07/01/20

  use EOS_Gas_module
  use EOS_Water_module

  implicit none

  PetscReal, intent(in)  :: t, p, ag, xmfnacl
  PetscReal, intent(out) :: mdb
  PetscReal :: aux(1)
  PetscInt  :: ierr
  PetscReal :: kgdw, mww, aw, xmfh2o, mwg

  !  Initialise

  ierr   = 0
  aux(1) = xmfnacl

  ! Set up mole fractions

  aw     = 1.0 - ag
  xmfh2o = 1.0 - xmfnacl

  ! Brine density

  call EOSWaterDensityExt(t ,p ,aux, kgdw ,mdb, ierr)

  ! Aqueous molecular weight

  mwg = EOSGasGetFMW()
  mww   = aw*(xmfh2o*FMWH2O + xmfnacl*FMWNACL) + ag*mwg

  ! Post processing calc. on brine density, e.g. Duan mixing of dissolved CO2

  call EOSWaterMixedDensity(t, p ,ag, xmfnacl, mww, kgdw, mdb, ierr)

end subroutine EOSBrineDensity

! *************************************************************************** !

subroutine checkErr(ierr)

  !
  ! Check for an MPI error
  !
  ! Author: Dave Ponting
  ! Date  : 03/29/19

  implicit none

  PetscInt,intent(in)::ierr

  if (ierr /= 0) as_MPI_errs = as_MPI_errs + 1

end subroutine checkErr

end module Aquifer_Solver_module
