module IO_Eclipse_Util_module

!  Module of routines which read industry-standard Eclipse files
!  Eclipse is a trademark of the Schlumberger Corporation

#include "petsc/finclude/petscsys.h"
  use petscsys
  use PFLOTRAN_Constants_module

  implicit none

  private

    PetscBool :: ewriter_rest_maps_set = PETSC_FALSE

  public :: SetupEwriterRestMaps, &
            AllocateLocalSolution, &
            DeleteLocalSolution, &
            AddSolution

contains

! ************************************************************************** !

subroutine SetupEwriterRestMaps(patch, grid, option)
  !
  ! Set up maps required to convert from Pflotran order to that
  ! required for Eclipse restart files
  !
  ! Author: Dave Ponting
  ! Date  : 01/15/19

  use Patch_module
  use Grid_module
  use Option_module
  use Output_Eclipse_module, only: setupRestMaps

  implicit none

  type(patch_type), pointer :: patch
  type(grid_type ), pointer :: grid
  type(option_type) :: option

  PetscInt :: nlmax, ngmax, lid, gid, aid, ierr
  PetscInt :: mlmax, mgmax

  PetscInt, allocatable :: ltoa(:), gtoa(:)

  if ( .not. ewriter_rest_maps_set ) then

    !  First, find the maximum value of nlmax & ngmax over all procs

    nlmax = grid%nlmax
    ngmax = grid%ngmax

    ierr  = 0
    call MPI_AllReduce(nlmax, mlmax, ONE_INTEGER_MPI, MPI_INTEGER, MPI_MAX, &
                       option%mycomm, ierr)

    call MPI_AllReduce(ngmax, mgmax, ONE_INTEGER_MPI, MPI_INTEGER, MPI_MAX, &
                       option%mycomm, ierr)

    !  Allocate local to active map and fill on this proc

    allocate(ltoa(mlmax))
    ltoa=0

    do lid = 1, nlmax
      gid = grid%nL2G(lid  )
      aid = grid%nG2A(gid)
      if (patch%imat(gid) <= 0) cycle
      ltoa(lid) = aid
    enddo

    !  Allocate global to active map and fill on this proc

    allocate(gtoa(mgmax))
    gtoa=0

    do gid = 1, ngmax
      aid = grid%nG2A(gid)
      if (patch%imat(gid) <= 0) cycle
      gtoa(gid) = aid
    enddo

    !  Setup the restart maps in Output_Eclipse_module

    call SetupRestMaps(ltoa, gtoa, option, nlmax, ngmax, mlmax, mgmax)

    !  Delete the ltoa work array

    deallocate(ltoa)
    deallocate(gtoa)

    ! Flag so not done again

     ewriter_rest_maps_set = PETSC_TRUE

  endif

end subroutine SetupEwriterRestMaps

!*****************************************************************************!

subroutine AllocateLocalSolution(vsol, nsol, zsol, nmax, option, &
                                 store_den, store_visc, store_kr, store_pc)
  !
  ! Allocate structures holding solutions for Output_Eclipse_module
  !
  ! Author: Dave Ponting
  ! Date  : 01/15/19

  use Option_module
  use PM_TOWG_Aux_module, only: towg_miscibility_model,towg_isothermal, &
                                towg_hysteresis_model
  use PM_TOilIms_Aux_module, only : toil_ims_isothermal

  implicit none

  PetscReal       , pointer,intent(out) :: vsol(:,:)
  PetscInt                 ,intent(out) :: nsol
  character(len=8), pointer,intent(out) :: zsol(:)
  PetscInt                  ,intent(in) :: nmax
  type(option_type)         ,intent(in) :: option
  PetscBool                 ,intent(in) :: store_den
  PetscBool                 ,intent(in) :: store_visc
  PetscBool                 ,intent(in) :: store_kr
  PetscBool                 ,intent(in) :: store_pc

  character(len=8), pointer :: zsolw(:)
  PetscInt  :: isol,nsolw,msolw

!  Set up counts and initial allocation of work array

  nsolw = 0
  msolw = 1
  allocate(zsolw(msolw))

! Basic solutions in both TOIL and TOWG modes

  call addsolution('PRESSURE',nsolw,msolw,zsolw)

! Always water
                    call addsolution('SWAT',nsolw,msolw,zsolw)
  if ( store_den  ) call addsolution('DWAT',nsolw,msolw,zsolw)
  if ( store_visc ) call addsolution('VWAT',nsolw,msolw,zsolw)
  if ( store_kr   ) call addsolution('KRW' ,nsolw,msolw,zsolw)
  if ( store_pc   ) call addsolution('PCW' ,nsolw,msolw,zsolw)

! Oil unless gas-water
  if ( .not. (      (option%iflowmode       == TOWG_MODE      ) &
              .and. (towg_miscibility_model == TOWG_GAS_WATER ) ) ) then
                      call addsolution('SOIL',nsolw,msolw,zsolw)
    if ( store_den  ) call addsolution('DOIL',nsolw,msolw,zsolw)
    if ( store_visc ) call addsolution('VOIL',nsolw,msolw,zsolw)
    if ( store_kr   ) call addsolution('KRO' ,nsolw,msolw,zsolw)
  endif

! Add optional solutions for TOIL_IMS

  if (option%iflowmode == TOIL_IMS_MODE) then
    if (.not. toil_ims_isothermal) call addsolution('TEMP',nsolw,msolw,zsolw)
  endif

! Add optional solutions for TOWG

  if (option%iflowmode == TOWG_MODE) then

                      call addsolution('SGAS',nsolw,msolw,zsolw)
    if ( store_den  ) call addsolution('DGAS',nsolw,msolw,zsolw)
    if ( store_visc ) call addsolution('VGAS',nsolw,msolw,zsolw)
    if ( store_kr   ) call addsolution('KRG' ,nsolw,msolw,zsolw)
    if ( store_pc   ) then
      if ( towg_miscibility_model /= TOWG_SOLVENT_TL ) then
        call addsolution('PCG' ,nsolw,msolw,zsolw)
      endif
    endif

    if (     towg_miscibility_model == TOWG_SOLVENT_TL &
        .or. towg_miscibility_model == TOWG_BLACK_OIL  ) then
      call addsolution('PSAT',nsolw,msolw,zsolw)
    endif

    if (     towg_miscibility_model == TOWG_SOLVENT_TL) then
                        call addsolution('SSLV',nsolw,msolw,zsolw)
      if ( store_den  ) call addsolution('DSLV',nsolw,msolw,zsolw)
      if ( store_visc ) call addsolution('VSLV',nsolw,msolw,zsolw)
      if ( store_kr   ) call addsolution('KRS' ,nsolw,msolw,zsolw)
    endif

    if (     towg_miscibility_model == TOWG_GAS_WATER) then
      call addsolution('AMFG',nsolw,msolw,zsolw)
      call addsolution('AMFW',nsolw,msolw,zsolw)
      call addsolution('YMFG',nsolw,msolw,zsolw)
      call addsolution('YMFW',nsolw,msolw,zsolw)
      if ( towg_hysteresis_model > 0 ) then
        call addsolution('SPNMAX' ,nsolw,msolw,zsolw)
        call addsolution('SPNSHFT',nsolw,msolw,zsolw)
        call addsolution('SPWSHFT',nsolw,msolw,zsolw)
        call addsolution('SPNTRAP',nsolw,msolw,zsolw)
      endif
    endif

    if (.not.towg_isothermal) call addsolution('TEMP',nsolw,msolw,zsolw)

  endif

! Allocate the storage

  nsol = nsolw
  allocate(zsol(nsol))
  allocate(vsol(nmax,nsol))

!  Copy over zsolw

   do isol=1,nsol
     zsol(isol) = zsolw(isol)
   enddo

!  Deallocate work array

   deallocate(zsolw)

end subroutine AllocateLocalSolution

! *************************************************************************** !

subroutine DeleteLocalSolution(vsol, zsol)
  !
  ! Delete structures holding solutions for Eclipse file I/O
  !
  ! Author: Dave Ponting
  ! Date  : 01/15/19

  implicit none

  PetscReal, pointer        :: vsol(:,:)
  character(len=8), pointer :: zsol(:)

  deallocate(vsol)
  deallocate(zsol)

end subroutine DeleteLocalSolution

!*****************************************************************************!

subroutine AddSolution(tsol, nsol, msol, zsol)
  !
  ! Add a solution name to the the list zsol, extending if required
  !
  ! Author: Dave Ponting
  ! Date  : 05/15/19

  implicit none

  character(len=*),          intent(in)    :: tsol
  PetscInt,                  intent(inout) :: nsol
  PetscInt,                  intent(inout) :: msol
  character(len=8), pointer, intent(inout) :: zsol(:)

  character(len=8), pointer :: zsoltemp(:)
  PetscInt :: msoltemp,i

! Check if zsol is full, and extend if so

  if ( nsol == msol ) then
    ! Store current list in temporary list
    msoltemp = msol
    allocate(zsoltemp(msoltemp))
    ! Copy over
    do i = 1, nsol
     zsoltemp(i)=zsol(i)
    enddo
    ! Free orignal list and re-allocate
    deallocate(zsol)
    msol = 2*msol
    allocate(zsol(msol))
    ! Copy back from temporary list
    do i = 1, nsol
     zsol(i)=zsoltemp(i)
    enddo
    ! Deallocate temporary list
    deallocate(zsoltemp)
  endif

! Add new item to list

  nsol = nsol + 1
  zsol(nsol) = tsol

end subroutine AddSolution

end module IO_Eclipse_Util_module
