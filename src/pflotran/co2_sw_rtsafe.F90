  module co2_sw_rtsafe_module

#include "petsc/finclude/petscsys.h"
      use petscsys
      use PFLOTRAN_Constants_module
      use Utility_module, only : Equal
  implicit none



  contains

! ************************************************************************** !

  FUNCTION rtsafe(funcd,x1,x2,xacc)

  IMPLICIT NONE
  PetscReal, INTENT(IN) :: x1,x2,xacc
  PetscReal :: rtsafe

! INTERFACE
!   SUBROUTINE funcd(x,fval,fderiv)
!   IMPLICIT NONE
!   PetscReal, INTENT(IN) :: x
!   PetscReal, INTENT(OUT) :: fval,fderiv
!   END SUBROUTINE funcd
! END INTERFACE

  external funcd
  INTEGER, PARAMETER :: MAXIT=100
  INTEGER :: j
  PetscReal :: df,dx,dxold,f,fh,fl,temp,xh,xl

  call funcd(x1,fl,df)
  call funcd(x2,fh,df)
  if ((fl > 0.0 .and. fh > 0.0) .or. &
    (fl < 0.0 .and. fh < 0.0)) &
    print *, 'root must be bracketed in rtsafe'
  if (Equal(fl,0.d0)) then
    rtsafe=x1
    RETURN
  else if (Equal(fh,0.d0)) then
    rtsafe=x2
    RETURN
  else if (fl < 0.0) then
    xl=x1
    xh=x2
  else
    xh=x1
    xl=x2
  end if
  rtsafe=0.5*(x1+x2)
  dxold=dabs(x2-x1)
  dx=dxold
  call funcd(rtsafe,f,df)
  do j=1,MAXIT
    if (((rtsafe-xh)*df-f)*((rtsafe-xl)*df-f) >= 0.0 .or. &
      dabs(2.0*f) > dabs(dxold*df) ) then
      dxold=dx
      dx=0.5*(xh-xl)
      rtsafe=xl+dx
      if (Equal(xl,rtsafe)) RETURN
    else
      dxold=dx
      dx=f/df
      temp=rtsafe
      rtsafe=rtsafe-dx
      if (Equal(temp,rtsafe)) RETURN
    end if
    if (dabs(dx) < xacc) RETURN
    call funcd(rtsafe,f,df)
    if (f < 0.0) then
      xl=rtsafe
    else
      xh=rtsafe
    end if
  end do
  print *,'rtsafe: exceeded maximum iterations'
  END FUNCTION rtsafe

! ************************************************************************** !

subroutine bracket(func,x1,x2)
  
  implicit none
  
  PetscInt :: i,ifind
  PetscReal :: fac,f1,f2,x1,x2,df
  
  external func
  
  fac = 1.2d0
  call func(x1,f1,df)
  call func(x2,f2,df)
  ifind = 1
  do i = 1, 200
    if (f1*f2 < 0.d0) return
    if (dabs(f1) < dabs(f2)) then
      x1 = x1+fac*(x1-x2)
      call func(x1,f1,df)
     else
       x2 = x2+fac*(x2-x1)
       call func(x2,f2,df)
     endif
  enddo
  ifind = 0
  print *,'root bracket failed',x1,x2,f1,f2
  return
end subroutine bracket

! ************************************************************************** !

function range_is_opp_signed(func,x1,x2)

  ! checks if func(x1) and func(x2) are of opposite sign, implying
  ! existence of a root between x1 and x2

  ! DS, Oct 2020

  implicit none
  
  PetscReal :: f1,f2,x1,x2,df
  PetscBool :: range_is_opp_signed
  
  external func

  call func(x1,f1,df)
  call func(x2,f2,df)

    if (f1*f2 < 0.d0)  then
      range_is_opp_signed = PETSC_TRUE
      return
    else
      range_is_opp_signed = PETSC_FALSE
      return
    endif

end function range_is_opp_signed

! ************************************************************************** !

subroutine possible_roots_between(func,x1,x2,x1s,x2s,no_roots)

  ! identifies up to 10 possible roots of func() between x1 and x2

  ! DS, Oct 2020
  
  implicit none
  
  PetscInt  :: i
  PetscReal :: x1,x2,df
  PetscReal :: x1s(1:10),x2s(1:10)
  PetscInt  :: no_roots

  PetscInt :: N
  PetscReal :: inc, xl, xu, fl, fu
  
  external func

  no_roots = 0
  N = 1000
  inc = (x2-x1)/N

  xl = x1
  call func(xl,fl,df)

  do i = 1,N
    xu = xl + inc
    call func(xu,fu,df)

    if (fl*fu < 0.d0) then
      no_roots = no_roots + 1
      if (no_roots < 11) then
        x1s(no_roots) = xl
        x2s(no_roots) = xu
      else
        ! note this is less than ideal error handling, a crash might
        ! be preferred. It is highly unlikely there will be more than
        ! two roots given the intended use of this routine
        print *, "CO2 SW database: more than 10 potential roots in range."
      endif !/ if no_roots < ...
    endif!/ if fu*fl < 0

    xl = xu
    fl = fu

  enddo
end subroutine possible_roots_between

! ************************************************************************** !

subroutine widen_if_necessary(func,x1,x2)

  ! simpler version of bracket()
  ! Used in the case that we know there is a root of func() between
  ! x1 and x2 at one p value, but there isn't at a perturbed p value
  ! - in which case the root must be close, so we widen the (x1,x2) 
  ! bracket very slightly

  ! DS, Oct 2020

  implicit none

  PetscReal :: x1,x2
  PetscReal :: f1,f2
  PetscInt :: maxits,i
  PetscReal :: df
  
  external func
  maxits = 10
  do i = 1,maxits
    call func(x1,f1,df)
    call func(x2,f2,df)
    if (f1*f2 < 0.d0) return
    x1 = x1 - 0.5d0*(x2-x1)
    x2 = x2 + 0.5d0*(x2-x1)

  enddo

end subroutine widen_if_necessary

! ************************************************************************** !


end module co2_sw_rtsafe_module
